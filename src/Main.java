import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) {

        String addressFileName = "E:\\AS_ADDR_OBJ.XML";
        String addressHierarchyFileName = "E:\\AS_ADM_HIERARCHY.XML";
        List<AddressHierarchyInfo> addressHierarchy = parseAddressHierarchyFile(addressHierarchyFileName);
        List<AddressObject> addressObjects = parseAddressObjectsFile(addressFileName);

        List<Long> testIds = new ArrayList<>();
        testIds.add(1422396L);
        testIds.add(1450759L);
        testIds.add(1449192L);
        testIds.add(1451562L);

        // Задача 1
        List<String> foundAddresses = findAddressByIdList(addressObjects, testIds, "2019-01-01");
        for (String address : foundAddresses) {
            System.out.println(address);
        }
        // Задача 2
        List<String> foundFullAddresses = findFullAddressByType(addressObjects, addressHierarchy, "проезд");
        for (String address : foundFullAddresses) {
            System.out.println(address);
        }
    }

    //****************************************************************************************************************
    //  Задача 1.
    //  Функция находит по списку идентификаторов актуальные на указанную дату адреса
    //  Дата нужно передавать в виде строки формата "2010-01-01"
    //
    //  Возвращает массив строк содержащих указанные адреса
    public static List<String> findAddressByIdList(List<AddressObject> addressObjects, List<Long> objectIds, String dateString) {
        List<String> result = new ArrayList<>();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date;
        try {
            date = dateFormat.parse(dateString);
        } catch (ParseException e) {
            System.out.println("Incorrect date format");
            return result;
        }
        result.addAll(addressObjects.stream()
                .filter(addressObject ->
                        //отфильтруем те адреса, которые имеют искомые objectId
                        objectIds.contains(addressObject.objectId)
                                //также проверим, что строка записи об адресе актуальна
                                && addressObject.isActual()
                                // + проверим что данные записи об адресе попадают в указанный временной период
                                && date.getTime() >= addressObject.startDate.getTime() && date.getTime() <= addressObject.endDate.getTime())
                .distinct()
                .map(AddressObject::toString)
                .collect(Collectors.toList()));
        return result;
    }
    //  Задача 1. Конец
    //
    //****************************************************************************************************************

    //****************************************************************************************************************
    //  Задача 2. Начало
    //  Функция возвращает массив полных адресов с учетом иерархии по указанному типу адреса
    //
    //  Т.к. в этом задании во входных параметрах ничего не было указано про дату, решил при поиске брать последний
    //  занесенный актуальный адрес (Max по полю startDate).

    public static List<String> findFullAddressByType(List<AddressObject> addressObjects, List<AddressHierarchyInfo> addressHierarchy, String type) {
        List<String> result = new ArrayList<>();
        //Ищем адреса с указанным типом
        Set<AddressObject> foundAddresses = addressObjects.stream()
                .filter(addressObject -> addressObject.typeName.equals(type))
                .collect(Collectors.toSet());
        //Для найденных адресов получаем формируем полный адрес с учетом иерархии
        for (AddressObject foundAddress : foundAddresses) {
            result.add(getFullAddressWithHierarchy(foundAddress, addressObjects, addressHierarchy));
        }
        return result;
    }

    private static String findFirstAddressById(List<AddressObject> addressObjects, long objectId) {
        Optional<String> result = addressObjects.stream()
                .filter(addressObject ->
                        //отфильтруем те адреса, которые имеют искомые objectId
                        addressObject.objectId == objectId
                                //также проверим, что строка записи об адресе актуальна
                                && addressObject.isActual())
                .map(addressObject -> addressObject.typeName + " " + addressObject.name)
                .findFirst();
        return result.orElse("");
    }

    private static String getFullAddressWithHierarchy(AddressObject currentAddress, List<AddressObject> addressObjects, List<AddressHierarchyInfo> addressHierarchy) {
        //Получаем текущий адрес
        String fullAddress = currentAddress.typeName + " " + currentAddress.name;
        //рекурсивно пробегаем по иерархии родителей и добавляем их адреса к нашему исходному адресу
        Optional<AddressHierarchyInfo> parent = getParent(addressHierarchy, currentAddress.objectId);
        while (parent.isPresent()) {
            String parentAddress = findFirstAddressById(addressObjects, parent.get().parentId);
            if (!parentAddress.isEmpty()) {
                fullAddress = parentAddress + ", " + fullAddress;
            }
            parent = getParent(addressHierarchy, parent.get().parentId);
        }
        return fullAddress;
    }

    //Т.к. про дату во втором задании сказано не было, беру максимальный актуальный
    private static Optional<AddressHierarchyInfo> getParent(List<AddressHierarchyInfo> addressHierarchy, long objectId) {
        return addressHierarchy.stream()
                .filter(addressHierarchyInfo ->
                        addressHierarchyInfo.isActive()
                                && addressHierarchyInfo.objectId == objectId
                ).max(Comparator.comparing(AddressHierarchyInfo::getStartDate))
                .stream().findFirst();
    }

    //  Задача 2. Конец
    //
    //****************************************************************************************************************

    //****************************************************************************************************************
    //  Служебные функции для парсинга входящих данных
    public static List<AddressHierarchyInfo> parseAddressHierarchyFile(String addressHierarchyFileName) {
        List<AddressHierarchyInfo> addressHierarchyInfos = new ArrayList<>();
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document document = builder.parse(addressHierarchyFileName);
            Element rootElement = document.getDocumentElement();
            NodeList itemNodes = rootElement.getElementsByTagName("ITEM");

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

            for (int i = 0; i < itemNodes.getLength(); i++) {
                Element item = (Element) itemNodes.item(i);
                addressHierarchyInfos.add(new AddressHierarchyInfo(
                        Long.parseLong(item.getAttribute("ID")),
                        Long.parseLong(item.getAttribute("OBJECTID")),
                        Long.parseLong(item.getAttribute("PARENTOBJID")),
                        dateFormat.parse(item.getAttribute("STARTDATE")),
                        dateFormat.parse(item.getAttribute("ENDDATE")),
                        Integer.parseInt(item.getAttribute("ISACTIVE"))
                ));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return addressHierarchyInfos;
    }

    public static List<AddressObject> parseAddressObjectsFile(String addressFileName) {
        List<AddressObject> addressObjects = new ArrayList<>();
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document document = builder.parse(addressFileName);
            Element rootElement = document.getDocumentElement();

            NodeList objectList = rootElement.getElementsByTagName("OBJECT");
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

            for (int i = 0; i < objectList.getLength(); i++) {
                Element objectElement = (Element) objectList.item(i);
                //TODO: Здесь не мешало бы добавить проверку полей. В приложенном файле в одной из строк нет поля NEXTID
                addressObjects.add(new AddressObject(
                        Long.parseLong(objectElement.getAttribute("ID")),
                        Long.parseLong(objectElement.getAttribute("OBJECTID")),
                        objectElement.getAttribute("NAME"),
                        objectElement.getAttribute("TYPENAME"),
                        dateFormat.parse(objectElement.getAttribute("STARTDATE")),
                        dateFormat.parse(objectElement.getAttribute("ENDDATE")),
                        Integer.parseInt(objectElement.getAttribute("ISACTUAL"))
                ));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return addressObjects;
    }

    static class AddressHierarchyInfo {
        long id;
        long objectId;
        long parentId;
        Date startDate;
        Date endDate;
        int isActive;

        public AddressHierarchyInfo(long id, long objectId, long parentId, Date startDate, Date endDate, int isActive) {
            this.id = id;
            this.objectId = objectId;
            this.parentId = parentId;
            this.startDate = startDate;
            this.endDate = endDate;
            this.isActive = isActive;
        }

        public Date getStartDate() {
            return startDate;
        }

        public boolean isActive() {
            return isActive > 0;
        }

        @Override
        public String toString() {
            return "AddressHierarchyInfo [objectId = " + objectId + ", parentId = " + parentId + " isActive = " + isActive + "]";
        }
    }

    static class AddressObject {
        private long id;
        private long objectId;
        private String name;
        private String typeName;
        private Date startDate;
        private Date endDate;
        private int isActual;

        public AddressObject(long id, long objectId, String name, String typeName, Date startDate, Date endDate, int isActual) {
            this.id = id;
            this.objectId = objectId;
            this.name = name;
            this.typeName = typeName;
            this.startDate = startDate;
            this.endDate = endDate;
            this.isActual = isActual;
        }

        public boolean isActual() {
            //У объекта два похожих поля: ISACTUAL, ISACTIVE – признаки актуальности адреса
            //Я оперся на ISACTUAL
            return isActual > 0;
        }

        @Override
        public String toString() {
            return "" + objectId + " : " + typeName + " " + name;
        }
    }

}