import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class MainTest {

    static List<Main.AddressHierarchyInfo> addressHierarchy;
    static List<Main.AddressObject> addressObjects;

    @org.junit.Before
    public void setUp() throws Exception {
        String addressFileName = "E:\\AS_ADDR_OBJ.XML";
        String addressHierarchyFileName = "E:\\AS_ADM_HIERARCHY.XML";
        addressHierarchy = Main.parseAddressHierarchyFile(addressHierarchyFileName);
        addressObjects = Main.parseAddressObjectsFile(addressFileName);
    }

    @Test
    public void test_findAddressByIdList() {

        List<Long> testIds = new ArrayList<>();
        testIds.add(1422396L);
        testIds.add(1450759L);
        testIds.add(1449192L);
        testIds.add(1451562L);

        List<String> foundAddresses = Main.findAddressByIdList(addressObjects, testIds, "2019-01-01");
        Assert.assertEquals(4, foundAddresses.size());
    }

    @Test
    public void test_findFullAddressByTypePassage() {
        List<String> foundFullAddresses = Main.findFullAddressByType(addressObjects, addressHierarchy, "проезд");
        Assert.assertEquals(13, foundFullAddresses.size());
    }

    @Test
    public void test_findFullAddressByTypeTerritory() {
        List<String> foundFullAddresses = Main.findFullAddressByType(addressObjects, addressHierarchy, "тер.");
        Assert.assertEquals(46, foundFullAddresses.size());
    }

}